<?php

namespace Database\Seeders;

use App\Models\Loyer;
use App\Models\Quartier;
use Illuminate\Database\Seeder;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class LoyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = public_path("/seeders/loyers.csv");
        $records = import_CSV($file);

        foreach ($records as $key => $record) {
            $quartier = Quartier::firstWhere("numero", $record['numero_quartier']);
            if($quartier instanceof Quartier){
                $polygon = Polygon::fromJson($record['geo_shape']);
                // dd($polygon);
                Loyer::create([
                    'quartier_id' => $quartier->id,
                    'secteur' => $record['secteur'],
                    'nb_pieces' => $record['nb_pieces'],
                    'epoque' => $record['epoque'],
                    'type' => $record['type'],
                    'loyer_reference' => $record['loyer_reference'],
                    'loyer_majore' => $record['loyer_majore'],
                    'loyer_minore' => $record['loyer_minore'],
                    'annee' => $record['annee'],
                    'geo_shape' => $polygon,
                    'geometry_x' => $record['geometry_x'],
                    'geometry_y' => $record['geometry_y'],
                ]);
            }
        }
    }
}
