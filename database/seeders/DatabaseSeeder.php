<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\PaysSeeder;
use Database\Seeders\QuartierSeeder;
use Database\Seeders\ArrondissementSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            PaysSeeder::class,
            ArrondissementSeeder::class,
            QuartierSeeder::class,
            LoyerSeeder::class,
        ]);
    }
}
