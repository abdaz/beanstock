<?php

namespace Database\Seeders;

use App\Models\Pays;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PaysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pays = Pays::create([
            "name" => "FRANCE",
            "code" => "FR",
        ])->villes()->create([
            "name" => "Paris"
        ]);
    }
}
