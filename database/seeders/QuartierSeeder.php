<?php

namespace Database\Seeders;

use App\Models\Arrondissement;
use App\Models\Quartier;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QuartierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = public_path("/seeders/quartiers.csv");
        $records = import_CSV($file);

        foreach ($records as $key => $record) {
            $arrondissement = Arrondissement::firstWhere("zip_code", $record['zip_code']);
            if($arrondissement instanceof Arrondissement){
                Quartier::create([
                    'arrondissement_id' => $arrondissement->id,
                    'n_sq_qu' => $record['n_sq_qu'],
                    'numero' => $record['numero'],
                    'c_quinsee' => $record['c_quinsee'],
                    'name' => $record['name'],
                    'perimetre' => $record['perimetre'],
                    'surface' => $record['surface'],
                    'coordonnee_x' => $record['coordonnee_x'],
                    'coordonnee_y' => $record['coordonnee_y'],
                ]);
            }
        }
    }
}
