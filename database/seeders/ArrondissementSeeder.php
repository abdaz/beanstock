<?php

namespace Database\Seeders;

use App\Models\Ville;
use App\Models\Arrondissement;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ArrondissementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ville = Ville::first();
        if($ville instanceof Ville){
            for($i=1; $i<=20; $i++) {
                Arrondissement::create([
                    'ville_id' => $ville->id,
                    'c_ar' => $i,
                    'n_sq_ar' => 750000000+$i,
                    'zip_code' => 75000+$i,
                ]);
            }
        }
    }
}
