<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loyers', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->foreignUuid("quartier_id");
            $table->integer("secteur");
            $table->integer("nb_pieces");
            $table->string("epoque");
            $table->enum("type", ["non meublé", "meublé"]);
            $table->decimal("loyer_reference");
            $table->decimal("loyer_majore");
            $table->decimal("loyer_minore");
            $table->year("annee");
            $table->polygon("geo_shape");
            $table->double("geometry_x");
            $table->double("geometry_y");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loyers');
    }
};
