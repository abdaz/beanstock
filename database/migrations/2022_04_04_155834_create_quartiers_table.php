<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quartiers', function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table->foreignUuid("arrondissement_id");
            $table->bigInteger("n_sq_qu");
            $table->bigInteger("numero");
            $table->bigInteger("c_quinsee");
            $table->string("name");
            $table->double("perimetre");
            $table->double("surface");
            $table->double("coordonnee_x");
            $table->double("coordonnee_y");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quartiers');
    }
};
