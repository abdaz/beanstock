<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SubmitTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_submiting_request()
    {
        $response = $this->post('/', [
            "coordinates" => "St-Germain-l'Auxerrois",
            "nb_rooms" => 2,
            "epoque" => "1971-1990",
            "annee" => "2020",
            "furnished" => "false",
        ]);
        $response->assertValid();
        $response->assertStatus(302);
        $response->assertSessionHasNoErrors();
    }
}
