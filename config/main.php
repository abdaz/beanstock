<?php

return [
    "google_geocode" => true,
    "epoques" => [
        "Je ne sais pas" => "Je ne sais pas",
        "Avant 1946" =>  "Avant 1946",
        "1946-1970" => "1946-1970",
        "1971-1990" => "1971-1990",
        "Apres 1990" => "Apres 1990",
    ],
    "nb_rooms_max" => 4,
    "annees_location" => [
        "2021" => "2021",
        "2020" => "2020",
        "2019" => "2019",
    ],
];
