<h5 class="mb-3">Renseignez les caractéristiques de votre logement</h5>
            <form method="POST" action="{{ route('submit') }}" class="needs-validation" >
                @csrf
                <div class="row g-3">
                    <div class="col-12">
                        <label for="coordinates" class="form-label">Coordonnées </label>
                        <input type="text" class="form-control" id="coordinates" name="coordinates" placeholder="Entrez vos coordonnées ou votre code postal..." value="{{ old("coordinates") }}" required>
                        @error("coordinates")
                        <small class="text-danger">
                            {{ $message }}
                        </small>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="nb_rooms" class="form-label">Nombre de pièces</label>
                        <select required name="nb_rooms" class="form-select" aria-label="Default select example" id="nb_rooms">
                            <option value="" selected hidden>Sélectionnez le nombre de pièces de votre logement</option>
                            @for ($i = 1; $i <= config("main.nb_rooms_max"); $i++)
                                <option {{ old("nb_rooms") == $i ? "selected" : "" }} value="{{ $i }}">{{ config("main.nb_rooms_max") == $i ? $i." et plus" : $i }}</option>
                            @endfor
                        </select>
                        @error("nb_rooms")
                        <small class="text-danger">
                            {{ $message }}
                        </small>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="epoque" class="form-label">Période de construction</label>
                        <select name="epoque" class="form-select" id="epoque" required>
                            <option value="" selected hidden>Sélectionnez la période de construction de votre logement</option>
                            @foreach (config("main.epoques") as $epoque => $libelle)
                                <option {{ old("epoque") == $epoque ? "selected" : "" }} value="{{ $libelle }}">{{ $libelle }}</option>
                            @endforeach
                        </select>
                        @error("epoque")
                        <small class="text-danger">
                            {{ $message }}
                        </small>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="construction_end_date" class="form-label">Votre logement est-il meublé ?</label><br>
                        <div class="form-check form-check-inline">
                            <input required {{ old("furnished") == "true" ? "checked" : "" }} class="form-check-input" type="radio" name="furnished" id="true" value="true">
                            <label class="form-check-label" for="true">OUI</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input required {{ old("furnished") == "false" ? "checked" : "" }} class="form-check-input" type="radio" name="furnished" id="false" value="false">
                            <label class="form-check-label" for="false">NON</label>
                        </div>
                        @error("furnished")
                        <small class="text-danger">
                            {{ $message }}
                        </small>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="annee" class="form-label">Année de location</label>
                        <select required name="annee" class="form-select" aria-label="Default select example" id="annee">
                            <option value="" selected hidden>Sélectionnez la période de construction de votre logement</option>
                            @foreach (config("main.annees_location") as $annee)
                                <option {{ old("annee") == $annee ? "selected" : "" }} value="{{ $annee }}">{{ $annee }}</option>
                            @endforeach
                        </select>
                        @error("annee")
                        <small class="text-danger">
                            {{ $message }}
                        </small>
                        @enderror
                    </div>
                </div>
                <button class="w-100 btn btn-primary btn-lg mt-4" type="submit">Vérifier</button>
            </form>