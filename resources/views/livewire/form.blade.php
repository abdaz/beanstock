<div class="row">
    <div class="col-md-6">
        <h5 class="mb-3">Renseignez les caractéristiques de votre logement</h5>
        <form class="needs-validation" novalidate>
            <div class="row g-3">
                <div class="col-12">
                    <label for="coordonnees" class="form-label">Coordonnées </label>
                    <input type="text" class="form-control" id="coordonnees" placeholder="Entrez vos coordonnées ou votre code postal" required>
                </div>
                <div class="col-12">
                    <label for="number_pieces" class="form-label">Nombre de pièces</label>
                    <input type="text" class="form-control" id="number_pieces" placeholder="1234 Main St" required>
                </div>
                <div class="col-12">
                    <label for="construction_start_date" class="form-label">Date de début de construction</label>
                    <input type="date" class="form-control" id="construction_start_date" placeholder="Date de début de construction">
                </div>
                <div class="col-12">
                    <label for="construction_end_date" class="form-label">Date de fin de construction</label>
                    <input type="date" class="form-control" id="construction_end_date" placeholder="Date de fin de construction">
                </div>

                <div class="col-12">
                    <label for="construction_end_date" class="form-label">Votre logement est-il meublé ?</label><br>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="furnished" id="true" value="true">
                        <label class="form-check-label" for="true">OUI</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="furnished" id="false" value="false">
                        <label class="form-check-label" for="false">NON</label>
                    </div>
                </div>
            </div>

            <button wire:click="submit" class="w-100 btn btn-primary btn-lg mt-4" type="button">Continue to checkout</button>
        </form>
    </div>
    <div class="col-md-6 d-flex align-items-center justify-content-center align-self-center">
        <div class="">
            <h5 class="mb">Les loyers de référence pour votre logement sont :</h5>
        </div>
    </div>
</div>