@extends("layouts.master")

@section("content")
<div class="content mt-2">
    <div class=" text-center">
        <img class="d-block mx-auto mb-4" src="{{ asset("images/logo.png") }}" alt="" height="67">
        <hr style="margin-top: -20px" class="hr">
    </div>
    <div class="row">
        <div class="col-md-6">
            @include("partials._form")
        </div>
        <div class="col-md-6 d-flex align-items-center justify-content-center align-self-center">
            <div class="ms-2">
                @if (session("error"))
                <div class="alert alert-danger" role="alert">
                    {{ session("error") }}
                </div>
                @endif
                @if(session("loyers") && session("loyers")->count()>0)
                <div class="">
                    <h5 class="mb">Les loyers de référence pour votre logement sont :</h5>
                </div>
                <div class="accordion" id="accordion">
                    @foreach (session("loyers") as $key => $loyer)
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="heading{{ $key }}">
                        <button class="accordion-button {{ session("loyers")->first()->id == $loyer->id ? '' : 'collapsed' }} text-left" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $key }}" aria-expanded="{{ session("loyers")->first()->id == $loyer->id ? 'true' : 'false' }}" aria-controls="collapse{{ $key }}">
                            <div>
                                <span>{{ $loyer->libelle }} </span>
                                <span class="text-success fw-bold">{{ $loyer->loyer_reference }} <small>€ au m²</small></span>
                            </div>
                        </button>
                      </h2>
                      <div id="collapse{{ $key }}" class="accordion-collapse collapse {{ session("loyers")->first()->id === $loyer->id ? 'show' : '' }}" aria-labelledby="heading{{ $key }}" data-bs-parent="#accordion">
                        <div class="accordion-body">
                          <ul>
                            <li>Loyer de référence minoré: <span>{{ $loyer->loyer_minore }} <small>€ au m²</small></span></li>
                            <li >Loyer de référence: <span class="text-success">{{ $loyer->loyer_reference }} <small>€ au m²</small></span></li>
                            <li>Loyer de référence majoré: <span>{{ $loyer->loyer_majore }} <small>€ au m²</small></span></li>
                        </ul>
                        </div>
                      </div>
                    </div>
                    @endforeach
                </div>
                @elseif(session("loyers") && session("loyers")->count()==0)
                <div class="alert alert-info" role="alert">
                    Aucun loyer de référence n'a été trouvé pour l'adresse « {{ old("coordinates") }} »
                </div>
                @else
                <h3 class="ms-5 lh-base">
                    Renseignez les caractéristiques de votre logement dans le formulaire à gauche, pour pouvoir avoir des informations sur les loyers de référence pour votre logement.
                </h3>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
