<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Form extends Component
{
    public $result;
    public $coordonnes;
    public $nb_pieces;
    public $meubles;

    public function render()
    {
        return view('livewire.form');
    }

    public function submit()
    {
        # code...
    }
}
