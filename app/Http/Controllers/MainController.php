<?php

namespace App\Http\Controllers;

use Throwable;
use App\Models\Loyer;
use App\Models\Quartier;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Geocoder\Geocoder;
use App\Models\Arrondissement;
use App\Http\Requests\SearchFormRequest;
use Exception;
use MatanYadaev\EloquentSpatial\Objects\Point;

class MainController extends Controller
{
    public function index()
    {
        return view("welcome");
    }

    public function submit(SearchFormRequest $request)
    {
        if(config("main.google_geocode")){
            try {
                $loyers = $this->googleGeocode($request->coordinates);
            } catch (\Throwable $th) {
                return redirect()->route("index")->with("error", $th->getMessage())->withInput();
            }
        }else{
            $loyers = $this->mannualyGeocode($request->coordinates);
        }

        $loyers = $this->filterLoyers($loyers, $request);
        return redirect()->back()->with("loyers", $loyers)->withInput();
    }

    public function filterLoyers($loyers, Request $request)
    {
        if($request->nb_rooms){
            $loyers = $loyers->where("nb_pieces", $request->nb_rooms);
        }
        if($request->furnished){
            $furnished = $request->furnished == "true" ? "meublé" : "non meublé";
            $loyers = $loyers->where("type", $furnished);
        }
        if($request->epoque && $request->epoque!="Je ne sais pas"){
            $loyers = $loyers->where("epoque", $request->epoque);
        }
        if($request->annee){
            $loyers = $loyers->where("annee", $request->annee);
        }

        return $loyers;
    }

    public function googleGeocode($coordinates)
    {
        $result = app('geocoder')->geocode($coordinates)->get()->first();
        if (! $result) {
            throw new Exception("Aucun loyer de référence trouvé pour l'adresse «".$coordinates."»");
        }
        $coordinates = $result->getCoordinates();
        $lat = $coordinates->getLatitude();
        $lng = $coordinates->getLongitude();
        $point = new Point($lat, $lng);
        return Loyer::whereContains('geo_shape', $point)->get();
    }

    public function mannualyGeocode($coordinates)
    {
        if (preg_match("/^(750)[0-2]{1}\d{1}$/", $coordinates) && strlen($coordinates)==5) {
            $zip = (int)$coordinates;
            if($zip < 70001 && $zip > 70020){
                return redirect()->route("index")->with("error", "Une erreur s'est produite");
            }
            $arrondissement = Arrondissement::firstWhere("zip_code", $zip);
            if(!$arrondissement){
                return redirect()->route("index")->with("error", "Une erreur s'est produite");
            }
            $loyers = $arrondissement->loyers;
        }elseif(preg_match("/^([-+]?)([\d]{1,2})(((\.)(\d+)(,)))(\s*)(([-+]?)([\d]{1,3})((\.)(\d+))?)$/", $coordinates)){
            $coordinates = Str::of($coordinates)->remove(" ")->explode(",");
            $point = new Point((float)$coordinates[0], (float)$coordinates[1]);
            $loyers = Loyer::whereContains('geo_shape', $point)->get();
        }else{
            $quartier = Quartier::where("name", "like", '%'.$coordinates.'%')->first();
            if (! $quartier) {
                return redirect(route("index"))->with("error", "Aucun prix de référence trouvé pour l'adresse renseignée");
            }
            $loyers = $quartier->loyers;
        }
        return $loyers;
    }
}
