<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SearchFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "coordinates" => "required|min:5",
            "nb_rooms" => "required|integer|digits:1",
            "epoque" => [
                "required",
                Rule::in(config("main.epoques")),
            ],
            "annee" => [
                "required",
                Rule::in(config("main.annees_location")),
            ],
            "furnished" => "required|in:true,false",
        ];
    }
}
