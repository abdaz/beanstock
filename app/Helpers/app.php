<?php
if(!function_exists("import_CSV")){
    function import_CSV($filename, $delimiter = ','){
        if(!file_exists($filename) || !is_readable($filename))
        return false;
        $header = null;
        $data = array();
        // fgetcsv($file);
        if (($handle = fopen($filename, 'r')) !== false){
          while (($row = fgetcsv($handle)) !== false){
            if(!$header)
              $header = $row;
            else
              $data[] = array_combine($header, $row);
          }
          fclose($handle);
        }
        return $data;
    }
}

if(!function_exists("get_distance")){
  function get_distance($latitude1, $longitude1, $latitude2, $longitude2) {  
    $earth_radius = 6371;

    $dLat = deg2rad($latitude2 - $latitude1);  
    $dLon = deg2rad($longitude2 - $longitude1);  

    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
    $c = 2 * asin(sqrt($a));  
    $d = $earth_radius * $c;  

    return $d;  
  }
}
