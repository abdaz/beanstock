<?php

namespace App\Models;

use App\Models\Loyer;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Quartier extends Model
{
    use HasFactory;
    use Uuids;
    
    public $fillable = [ "arrondissement_id", "n_sq_qu", "c_qu", "numero", "c_quinsee", "name", "perimetre", "surface", "coordonnee_x", "coordonnee_y"];


    public function loyers()
    {
        return $this->hasMany(Loyer::class);
    }

}
