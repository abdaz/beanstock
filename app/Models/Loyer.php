<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use MatanYadaev\EloquentSpatial\SpatialBuilder;
use MatanYadaev\EloquentSpatial\Objects\Polygon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Loyer extends Model
{
    use HasFactory;
    use Uuids;
    public $fillable = [ "quartier_id", "nb_pieces", "epoque", "type", "loyer_reference", "loyer_majore", "loyer_minore", "annee", "geo_shape", "geometry_x", "geometry_y"];

    protected $casts = [
        'geo_shape' => Polygon::class,
    ];
    
    public function newEloquentBuilder($query): SpatialBuilder
    {
        return new SpatialBuilder($query);
    }

    public function getLibelleAttribute()
    {
        // dd($this->rooms);
        return "Quartier ".$this->quartier->name.", pour ".$this->rooms.", ".$this->type.", construit ".$this->periode." : ";
    }

    public function getRoomsAttribute()
    {
        return $this->nb_pieces." pièce".($this->nb_pieces > 1 ? "s" : "");
    }

    public function getPeriodeAttribute()
    {
        $str = Str::substr($this->epoque, 0, 5) ;
        return ($str == "Après" || $str == "Avant") ? $this->epoque : "Entre ".$this->epoque;
    }

    public function quartier()
    {
        return $this->belongsTo(Quartier::class);
    }
}
