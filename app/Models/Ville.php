<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ville extends Model
{
    use HasFactory;
    use Uuids;
    
    public $fillable = [ "name", "pays_id" ];

    public function pays(){
        return $this->belongsTo(Pays::class);
    }
}
