<?php

namespace App\Models;

use App\Models\Loyer;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Arrondissement extends Model
{
    use HasFactory;
    use Uuids;
    
    public $fillable = [
        "ville_id", "c_ar", "n_sq_ar", "zip_code"
    ];

    public function loyers()
    {
        return $this->hasManyThrough(Loyer::class, Quartier::class);
    }
}
